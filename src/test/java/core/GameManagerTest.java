package core;

import Interfaces.IPlayer;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit test of {@link GameManager}.
 */

public class GameManagerTest {

    @Test
    public void testCheckWinner(){

        GameManager TestGameManager1 = new GameManager();
        TestGameManager1.getPlayerH().getOwnFields();
        TestGameManager1.getPlayerC().getOwnFields();

        for (int i = 0; i < 7; i++) { TestGameManager1.getPlayerC().placeShip(0,i); }

        for (int i = 0; i < 7; i++) { TestGameManager1.getPlayerC().placeShip(1,i); }

        for (int i = 0; i < 7; i++) { TestGameManager1.getPlayerH().placeShip(0,i); }

        for (int i = 0; i < 7; i++) { TestGameManager1.getPlayerH().placeShip(1,i); }


        for (int i = 0; i < 7; i++) { TestGameManager1.getPlayerC().throwBomb(0, i, TestGameManager1.getPlayerH().getOwnFields()); }

        for (int i = 0; i < 7; i++) { TestGameManager1.getPlayerC().throwBomb(1,i, TestGameManager1.getPlayerH().getOwnFields()); }


        assertTrue("",TestGameManager1.checkWinner() == GameManager.Score.LOSE);


        /*
        public Score checkWinner(IPlayer playerH, IPlayer playerC){
        if (playerH.getHits() == 14 && playerC.getHits() == 14)
            return Score.TIE;
        else if (playerH.getHits() == 14)
            return Score.WIN;
        else if (playerC.getHits() == 14)
            return Score.LOSE;
        else return Score.CONTINUE;
        */

    }
}
