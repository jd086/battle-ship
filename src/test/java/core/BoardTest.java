package core;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit test of {@link Board}.
 */
public class BoardTest {


    @Test
    public void testCheckField (){

        Board TestBoard = new Board();
        FieldType[][] testFields = new FieldType[3][3];
        for (int i = 0; i <= 2; i++){
            for (int j = 0; j <= 2; j++) {
                testFields[i][j] = FieldType.W;
            }
        }
        testFields[0][0] = FieldType.S;
        testFields[1][1] = FieldType.X;
        testFields[2][2] = FieldType.O;

//teste auf nicht existierende feld
        assertTrue("Possible to throw a Bomb on Ship", TestBoard.checkField(0,0,testFields));
        assertTrue("Possible to throw a Bomb on Water", TestBoard.checkField(0,1,testFields));
        assertFalse("Not possible to throw a Bomb on Hit", TestBoard.checkField(1,1,testFields));
        assertFalse("Not possible to throw a Bomb on Hit", TestBoard.checkField(2,2,testFields));


    }




    }
