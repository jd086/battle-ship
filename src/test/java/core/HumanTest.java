package core;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit test of {@link Human}.
 */
public class HumanTest {

//teste auf nicht existierende feld

    @Test
    public void testplaceShip(){
        Human TestHuman0 = new Human(0);
        Human TestHuman3 = new Human(3);

        assertTrue("Human with 3 Ships can place 1 Ship ",TestHuman3.placeShip(1, 1));
        assertTrue("Human with 3 Ships can place 2 Ships ",TestHuman3.placeShip(1, 2));
        assertTrue("Human with 3 Ships can place 3 Ships ",TestHuman3.placeShip(2, 1));
        assertFalse("Human with 3 Ships can not place 4 Ships ",TestHuman3.placeShip(0, 2));

        assertFalse("Human with 0 Ships can not place Ships",TestHuman0.placeShip(1, 1));

        assertFalse("Non existing Field",TestHuman0.placeShip(100, 100));

    }

    @Test
    public void testFields(){

        Board TestBoard = new Board();
        FieldType[][] testFields = new FieldType[7][7];
        for (int i = 0; i <= 6; i++){
            for (int j = 0; j <= 6; j++) {
                testFields[i][j] = FieldType.W;
            }
        }

        Human TestHuman = new Human(14);
        TestHuman.placeShip(0,0);

        testFields[0][0] = FieldType.S;

        assertArrayEquals(testFields,TestHuman.getOwnFields());
    }


}
