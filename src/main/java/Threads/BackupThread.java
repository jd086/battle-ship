package Threads;

import GUI.ControllerMatch;
import extensions.Backup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BackupThread implements Runnable{

    private String threadName;
    private static Logger log = LogManager.getLogger(BackupThread.class);

    public BackupThread(String name) {
        threadName = name;
        log.debug("Creating " + threadName);
    }

    public void run(){
        log.debug("Running " + threadName);
        Backup b = new Backup();

        b.setFieldsHuman(ControllerMatch.getGameManager().getPlayerH().getOwnFields());
        b.setFieldsComputer(ControllerMatch.getGameManager().getPlayerC().getOwnFields());
        b.setScore(ControllerMatch.getScore());
        b.setHitsHuman(ControllerMatch.getGameManager().getPlayerH().getHits());
        b.setHitsComputer(ControllerMatch.getGameManager().getPlayerC().getHits());

        log.debug("Backup of current score created: Score: " + b.getScore() + ", Hits Human: " + b.getHitsHuman() + ", Hits Computer: " + b.getHitsComputer());

        ControllerMatch.setFinishedBackup(true);
    }

    public void start() {
        log.debug("Starting " +  threadName );

       Thread t = new Thread(this, threadName);
       t.start();
    }

}
