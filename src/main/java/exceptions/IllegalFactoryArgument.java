package exceptions;

public class IllegalFactoryArgument extends Exception {
    public IllegalFactoryArgument( String message) {super(message); }
}
