package GUI;

import extensions.ReadJSONFile;
import extensions.User;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class ControllerLogin implements Initializable {

    private static Logger logAcc = LogManager.getLogger("Account");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ReadJSONFile.readFile();
    }

    @FXML
    private void goToSingUp(MouseEvent event) throws IOException {
        Gui_starter.getApplication().setScene("/Fxml/fxml_sing_up.fxml", "Battle Ship");
    }

    @FXML
    TextField usernameField;

    @FXML
    PasswordField passwordField;

    @FXML
    Text errorMessageUsernameInput;

    @FXML
    Text errorMessagePasswordInput;

    private boolean verifyUserInput(String input) {
        String trimmmed = input.trim();
        return (trimmmed.length() == 0);
    }

    @FXML
    private void checkValidUsernameInput(KeyEvent event) {

        if (verifyUserInput(usernameField.getText())) errorMessageUsernameInput.setVisible(true);
        else errorMessageUsernameInput.setVisible(false);
        enableLoginButton();
    }

    @FXML
    private void checkValidPasswordInput(KeyEvent event) {
        if (verifyUserInput(passwordField.getText())) errorMessagePasswordInput.setVisible(true);
        else errorMessagePasswordInput.setVisible(false);
        enableLoginButton();
    }

    @FXML
    Button loginButton;

    private void enableLoginButton() {
        if (usernameField.getText().length()!=0 && passwordField.getText().length()!=0) loginButton.setDisable(false);
    }

    @FXML
    private void login(MouseEvent event) throws IOException{
        List<User> userStream = User.getUserList().parallelStream()
                .filter(u -> usernameField.getText().trim().equals(u.getUsername()))
                .collect(Collectors.toList());

        if (userStream.isEmpty()) {
            errorMessageUsernameInput.setVisible(true);
            errorMessageUsernameInput.setText("username not found");
        }
        else {
            if(!userStream.get(0).verifyPassword(passwordField.getText())) {
                errorMessagePasswordInput.setVisible(true);
                errorMessagePasswordInput.setText("wrong password");
            }

            if (userStream.get(0).getUsername().equals(usernameField.getText()) && userStream.get(0).getPassword().equals(passwordField.getText())) {
                ControllerMatch.setCurrentUser(userStream.get(0));
                logAcc.debug("Login successful - current User is set to " + userStream.get(0).toString());
                Gui_starter.getApplication().setScene("/Fxml/fxml_welcome.fxml", "Battle Ship");
            }
        }
    }
}



