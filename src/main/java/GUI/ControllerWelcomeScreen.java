package GUI;

import core.GameManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerWelcomeScreen implements Initializable {

    private static Logger log = LogManager.getLogger(ControllerMatch.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        welcomeText.setText("Hello " + ControllerMatch.getCurrentUser().getUsername() + ", let's play!");
    }

    @FXML
    Label welcomeText;

    @FXML
    private void goToLogin(MouseEvent event) throws IOException {
        Gui_starter.getApplication().setScene("/Fxml/fxml_login.fxml", "Battle Ship");
    }

    @FXML
    private void start(MouseEvent event) throws IOException {   // Exceptions checken !!!
        GameManager manager = new GameManager();
        ControllerMatch.setGameManager(manager);
        Gui_starter.getApplication().setScene("/Fxml/fxml_start.fxml", "Battle Ship");
        log.info("Game started - ready to place Ships");
    }
}
