package GUI;

import Threads.BackupThread;

import extensions.User;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.FieldType;
import core.GameManager;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import java.io.IOException;
import javafx.util.Duration;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.scene.Node;

import javafx.event.EventHandler;
import java.net.URL;


import static javafx.scene.paint.Color.DODGERBLUE; //#1e90ff


public class ControllerMatch implements Initializable{

    private static GameManager gameManager;
    private static User currentUser;
    private static GameManager.Score score;
    private static boolean finishedBackup;

    private static Logger log = LogManager.getLogger(ControllerMatch.class);

    public static synchronized GameManager getGameManager() {
        return gameManager;
    }
    public static User getCurrentUser() {
        User userTemp = new User(currentUser);
        return userTemp; }
    public static GameManager.Score getScore() { return score; }

    static void setGameManager(GameManager manager) { gameManager = manager; }
    static void setCurrentUser(User user) { currentUser = user; }
    public static void setFinishedBackup(boolean finished) { finishedBackup = finished; }

    public ControllerMatch() { }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        user.setText("Current user: " + currentUser.getUsername());
        points.setText("Points: " + currentUser.getPoints());
    }

    @FXML
    private void goToLogin(MouseEvent event) throws IOException{
        Gui_starter.getApplication().setScene("/Fxml/fxml_login.fxml", "Battle Ship");
    }

    @FXML
    private Text textShipLeft;

    @FXML
    private Button playButton;

    @FXML
    private void clickRectangle(MouseEvent event) {
        Rectangle rectangle = (Rectangle) event.getSource();
        Node node = (Node) event.getSource();
        int x = GridPane.getRowIndex(node);
        int y = GridPane.getColumnIndex(node);


        if (rectangle.getFill().equals(DODGERBLUE) && gameManager.getPlayerH().placeShip(x,y)) {
            rectangle.setFill(Color.web("#684e45"));
        } else {
            gameManager.getPlayerH().removeShip(x, y);
            rectangle.setFill(DODGERBLUE);
        }

        textShipLeft.setText("You have " + gameManager.getPlayerH().getQuantityShips() + "/14 Ships left");

        if (gameManager.getPlayerH().getQuantityShips() == 0) playButton.setDisable(false);
        else playButton.setDisable(true);
    }

    @FXML
    private GridPane boardGridH;

    @FXML
    private Text user;

    @FXML
    private Text points;

    @FXML
    private void play(MouseEvent event) throws IOException{
        Gui_starter.getApplication().setScene("/Fxml/fxml_game.fxml", "Battle Ship");

        FXMLLoader loader = Gui_starter.getLoader();
        ControllerMatch controller = loader.getController();

        boardGridH = controller.boardGridH;

        ObservableList<Node> children = boardGridH.getChildren();
        FieldType[][] fields = gameManager.getPlayerH().getOwnFields();
        for (Node node : children) {

            int x = GridPane.getRowIndex(node);
            int y = GridPane.getColumnIndex(node);

            if (fields[x][y] == FieldType.W) ((Rectangle) node).setFill(Color.web("#94caff"));
            else ((Rectangle) node).setFill(Color.web("#a88d83"));
        }

        log.info("Boards loaded - ready to play");

        finishedBackup = true;
    }

    @FXML
    private Text hitCounter;


    private EventHandler<MouseEvent> filter = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            // TODO Auto-generated method stub
            event.consume();
        }
    };

    @FXML
    private void clickToThrow(MouseEvent event){

            if (getGameManager().getPlayerH().getRoundCounter() != 0) finishedBackup = false;

            Node node = (Node) event.getSource();
            Rectangle rectangle = (Rectangle) event.getSource();
            int x = GridPane.getRowIndex(node);
            int y = GridPane.getColumnIndex(node);

            getGameManager().getPlayerC().setOwnFields(getGameManager().getPlayerH().throwBomb(x, y, getGameManager().getPlayerC().getOwnFields()));
            log.debug("Hits Human: " + getGameManager().getPlayerH().getHits());

            FieldType[][] fieldsC = getGameManager().getPlayerC().getOwnFields();

            boolean alreadyColored = rectangle.getFill().equals(Color.web("#684e45")) || rectangle.getFill().equals(DODGERBLUE);
            if (fieldsC[x][y] == FieldType.X) rectangle.setFill(Color.web("#684e45"));
            else rectangle.setFill(DODGERBLUE);


            hitCounter.setText(getGameManager().getPlayerH().getHits() + "/14");

            if (!alreadyColored) {

                boardGridC.addEventFilter(MouseEvent.ANY, filter);

                Timeline timeline = new Timeline(new KeyFrame(
                        Duration.millis(2000),
                        ae -> updateComputerGUI()));
                timeline.play();
            }
        }

    @FXML
    private GridPane boardGridC;

    private void updateComputerGUI() {

        Random random = new Random();
        boolean canThrow = true;

        while (canThrow) {
            int x = random.nextInt(7);
            int y = random.nextInt(7);

            if (getGameManager().getBoardGame().checkField(x, y, getGameManager().getPlayerH().getOwnFields())) {
                getGameManager().getPlayerH().setOwnFields(getGameManager().getPlayerC().throwBomb(x, y, getGameManager().getPlayerH().getOwnFields()));

                FieldType[][] fieldsH = getGameManager().getPlayerH().getOwnFields();
                ObservableList<Node> children = boardGridH.getChildren();

                for (Node node : children) {
                    int x2 = GridPane.getRowIndex(node);
                    int y2 = GridPane.getColumnIndex(node);

                    if (x2 == x && y2 == y) {
                        if (fieldsH[x][y] == FieldType.X) ((Rectangle) node).setFill(Color.web("#684e45"));
                        else ((Rectangle) node).setFill(DODGERBLUE);
                        break;
                    }
                }

                canThrow = false;
            }
        }

        score = getGameManager().checkWinner();
        setEndScreen();

        log.debug("Hits Computer: " + getGameManager().getPlayerC().getHits());

        BackupThread backupThread = new BackupThread("Backup Thread");
        backupThread.start();

        while(!finishedBackup) {
            Thread.onSpinWait();
        }

        boardGridC.removeEventFilter(MouseEvent.ANY, filter);

    }

    private void setEndScreen() {
        if (score != GameManager.Score.CONTINUE) {
            try {
                Gui_starter.getApplication().setScene("/Fxml/fxml_endScreen.fxml", "Battle Ship");
            } catch (IOException e) {
                log.error("Couldn't set end screen");
            }
        }
    }
}
