package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.stage.Stage;

import java.io.IOException;

public class Gui_starter extends Application{

    private static Gui_starter application;
    private static Stage stage = null;

    private static Logger log = LogManager.getLogger(Gui_starter.class);

    //current Controller
    private static FXMLLoader fxmlloader = null;

    static Gui_starter getApplication() {
        return application;
    }
    static FXMLLoader getLoader(){ return fxmlloader; }

    /** JDK 13 necessary!!! **/
    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage stage) throws Exception {
        log.info("Application started");
        application = this;
        Gui_starter.stage = stage;
        setScene("/Fxml/fxml_login.fxml", "Battle Ship");
        stage.setResizable(false);
        stage.getIcons().add(new Image(Gui_starter.class.getResourceAsStream("/Media/Icon.png")));
        stage.show();
    }

    void setScene(String fxml, String title) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
        Parent root = loader.load();
        Scene scene = new Scene(root, 1000, 600);
        log.debug("FXML-File of " + fxml + " loaded");
        stage.setScene(scene);
        stage.setTitle(title);

        fxmlloader = loader;
    }
}
