package GUI;

import core.GameManager;
import extensions.CreateJSONFile;
import extensions.User;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ControllerEndScreen implements Initializable {

    private static Logger logAcc = LogManager.getLogger("Account");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setEndText();
        user.setText("Current user: " + ControllerMatch.getCurrentUser().getUsername());
        points.setText("Points: " + ControllerMatch.getCurrentUser().getPoints());
        List<User> userList = User.getUserList();
        for (int i = 0; i < userList.size(); i++){
            if (userList.get(i).getUsername().equals(ControllerMatch.getCurrentUser().getUsername())) userList.remove(i); ;
        }
        userList.add(ControllerMatch.getCurrentUser());
        User.setUserList(userList);
        CreateJSONFile.write();
    }

    @FXML
    private void goToLogin(MouseEvent event) throws IOException{
        Gui_starter.getApplication().setScene("/Fxml/fxml_login.fxml", "Battle Ship");
    }

    @FXML
    public Text textScore;

    @FXML
    private Text user;

    @FXML
    private Text points;


    private void setEndText(){
        GameManager.Score score = ControllerMatch.getScore();
        User user = ControllerMatch.getCurrentUser();

        if (score == GameManager.Score.WIN) {
            textScore.setText("You WON!");
            user.setPoints(user.getPoints() + 100);
            ControllerMatch.setCurrentUser(user);
        }
        else if (score == GameManager.Score.LOSE) {
            textScore.setText("You LOST!");
        }
        else {
            textScore.setText("It's a TIE");
            user.setPoints(user.getPoints() + 50);
            ControllerMatch.setCurrentUser(user);
        }
    }

    @FXML
    private void startNewGame(MouseEvent event) throws IOException {
        Gui_starter.getApplication().setScene("/Fxml/fxml_welcome.fxml", "Battle Ship");
    }
}

