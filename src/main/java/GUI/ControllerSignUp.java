package GUI;

import extensions.CreateJSONFile;
import extensions.User;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;


public class ControllerSignUp {

    private static Logger logAcc = LogManager.getLogger("Account");

    @FXML
    TextField usernameInput;

    @FXML
    TextField passwordInput1;

    @FXML
    TextField passwordInput2;

    @FXML
    Label userInputErrorMessage;

    @FXML
    Label passwordInputErrorMessage1;

    @FXML
    Label passwordInputErrorMessage2;


    @FXML
    private void correctPasswordInput(MouseEvent event) throws IOException{
        if (!passwordInput1.getText().equals(passwordInput2.getText())) {
            passwordInputErrorMessage2.setVisible(true);
            passwordInputErrorMessage2.setText("Please make sure both passwords match");
        } else {
            passwordInputErrorMessage2.setVisible(false);
            assignCurrentUser();
            Gui_starter.getApplication().setScene("/Fxml/fxml_welcome.fxml", "Battle Ship");
        }
    }

    @FXML
    private void checkUsernameAlreadyUsed(KeyEvent event) {
        List<User> userStream = User.getUserList().parallelStream()
                .filter(u -> usernameInput.getText().trim().equals(u.getUsername()))
                .collect(Collectors.toList());

        if (!userStream.isEmpty()) {
            userInputErrorMessage.setVisible(true);
            userInputErrorMessage.setText("Username is already taken");
        } else userInputErrorMessage.setVisible(false);

        if (verifyUserInput(usernameInput.getText())) {
            userInputErrorMessage.setVisible(true);
            userInputErrorMessage.setText("Please insert valid username");
        }
    }

    @FXML
    private void checkPasswordLength(KeyEvent event){
        if(passwordInput1.getText().length() <= 3) {
            passwordInputErrorMessage1.setVisible(true);
            passwordInputErrorMessage1.setText("Password has to be at least 3 characters long");
        } else passwordInputErrorMessage1.setVisible(false);
    }

    private void assignCurrentUser(){
        User user = new User(usernameInput.getText().trim(), passwordInput1.getText(), 0);
        List<User> userlist = User.getUserList();
        userlist.add(user);
        User.setUserList(userlist);
        ControllerMatch.setCurrentUser(user);
        logAcc.debug("Sign-up successful - current user is set to " + user.toString());
        CreateJSONFile.write();
    }

    private boolean verifyUserInput(String input) {
        String trimmmed = input.trim();
        return (trimmmed.length() == 0);
    }





}



