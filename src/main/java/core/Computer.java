package core;

import Interfaces.IPlayer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Computer implements IPlayer {
    private int hit;
    private int quantityShips; // starts with 0 --> increases
    private int roundCounter;
    private FieldType[][] ownFields;

    private static Logger log = LogManager.getLogger(Computer.class);

    public Computer() {
        Board board = new Board();
        ownFields = board.createFields();
        quantityShips = 0;
        fillFields();
        hit = 0;
        roundCounter = 0;
    }

    public void setOwnFields(FieldType[][] fields)
    {
        ownFields = fields;
    }

    public FieldType[][] getOwnFields() {
        FieldType[][] temporary = new FieldType[7][7];
        for (int i = 0; i < ownFields.length; i++) {
            temporary[i] = ownFields[i].clone();
        }
        return temporary;
    }

    private int randomNumber() {
        Random random = new Random();
        return random.nextInt(7);
    }

    private void fillFields() {
        while (quantityShips < 14) {                    // 14 Ships
            placeShip(randomNumber(), randomNumber());
        }
    }

    public boolean placeShip(int x, int y){
        if (quantityShips <= 14 && ownFields[x][y] != FieldType.S) {
            ownFields[x][y] = FieldType.S;
            quantityShips++;
        }
        return true;
    }

    public void removeShip(int x, int y){
        if (ownFields[x][y] == FieldType.S) ownFields[x][y] = FieldType.W;
    }

    public FieldType[][] throwBomb (int x, int y, FieldType[][] fields){
        Board b = new Board();
        if (b.checkField(x, y, fields)) {
            if (fields[x][y] == FieldType.S) hit++;
            fields = b.adjustField(x, y, fields);
            roundCounter++;
            log.debug("Bomb thrown by Computer");
        }
        return fields;
    }

    public int getHits(){
        return hit;
    }
    public int getRoundCounter(){
        return roundCounter;
    }
    public int getQuantityShips(){
        return quantityShips;
    }

}
