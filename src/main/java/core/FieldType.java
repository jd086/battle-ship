package core;

public enum FieldType {

    W ('w'), //Water
    S ('s'), //Ship
    X ('x'), //Hit
    O ('o'); //No Hit

    char status;

    FieldType(char status) {
        this.status = status;
    }
}
