package core;

public class Board {

    public FieldType[][] createFields() {
        FieldType[][] fields = new FieldType[7][7];
        for (int i = 0; i <= 6; i++){
            for (int j = 0; j <= 6; j++) {
                fields[i][j] = FieldType.W;
            }
        }
        return fields;
    }

    public boolean checkField (int x, int y, FieldType[][] fields) {

        switch (fields[x][y])
        {
            case S: return true;
            case W: return true;
            default: return false;
        }
    }


    public FieldType[][] adjustField(int x, int y, FieldType[][] fields) {

        switch (fields[x][y])
        {
            case S: fields[x][y] = FieldType.X; break;
            case W: fields[x][y] = FieldType.O; break;
        }
        return fields;
    }

}
