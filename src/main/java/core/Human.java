package core;

import Interfaces.IPlayer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Human implements IPlayer {

    private int quantityShips; // starts with 14 --> decreases
    private FieldType[][] ownFields;
    private int hit;
    private int roundCounter;

    private static Logger log = LogManager.getLogger(Human.class);
    private static Logger threadLog = LogManager.getLogger("GameThreads");


    public Human(int quantityShips) {
        Board board = new Board();
        ownFields = board.createFields();
        this.quantityShips = quantityShips;
        hit = 0;
        roundCounter = 0;
    }

    public void setOwnFields(FieldType[][] fields) {
        ownFields = fields;
    }

    public FieldType[][] getOwnFields() {
        FieldType[][] temporary = new FieldType[7][7];
        for (int i = 0; i < ownFields.length; i++) {
            temporary[i] = ownFields[i].clone();
        }
        return temporary;
    }

    public boolean placeShip(int x, int y){
        if (quantityShips > 0 && ownFields[x][y]!= FieldType.S) {
            ownFields [x][y] = FieldType.S;
            quantityShips--;
            log.debug("Ship placed: " + quantityShips + " Ships left");
            return true;
        } else {
            log.warn("Couldn't place ship"); // if quantityShips == 0
            return false;
        }
    }

    public void removeShip(int x, int y){
        if (ownFields[x][y] == FieldType.S) {
            ownFields[x][y] = FieldType.W;
            quantityShips++;
            log.debug("Ship removed: " + quantityShips + " Ships left");
        }
    }

    public FieldType[][] throwBomb (int x, int y, FieldType[][] fieldsC){
        Board b1 = new Board();
        int i = 5;
        if (b1.checkField(x, y, fieldsC))
        {
            if (fieldsC[x][y] == FieldType.S) hit++;
            fieldsC = b1.adjustField(x, y, fieldsC);
            roundCounter++;
            threadLog.debug("Bomb thrown by Human");
        }
        return fieldsC;
    }


    public int getHits(){
        return hit;
    }

    public int getRoundCounter(){ return roundCounter; }

    public int getQuantityShips(){ return quantityShips; }

}
