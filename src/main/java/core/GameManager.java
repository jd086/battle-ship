package core;

import Interfaces.IPlayer;
import exceptions.IllegalFactoryArgument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import extensions.PlayerFactory;

public class GameManager {
    private IPlayer playerH;
    private IPlayer playerC;
    private Board boardGame;

    private static Logger log = LogManager.getLogger(GameManager.class);

    public GameManager() {
        boardGame = new Board();
        try {
            playerH = PlayerFactory.createPlayer(IPlayer.PlayerGroups.HUMAN);
            playerC = PlayerFactory.createPlayer(IPlayer.PlayerGroups.COMPUTER);
        } catch (IllegalFactoryArgument i) {
            log.fatal(i.toString());
            System.exit(1);
        }
        log.info("Game Manager created");
    }

    public synchronized IPlayer getPlayerH() { return playerH; }

    public synchronized IPlayer getPlayerC() { return playerC; }

    public synchronized Board getBoardGame() { return boardGame; }


    public enum Score {
        WIN,
        LOSE,
        TIE,
        CONTINUE;

        Score() {}
    }

    public Score checkWinner(){
        if (playerH.getHits() == 14 && playerC.getHits() == 14)
            return Score.TIE;
        else if (playerH.getHits() == 14)
            return Score.WIN;
        else if (playerC.getHits() == 14)
            return Score.LOSE;
        else return Score.CONTINUE;
    }
}
