package extensions;
import GUI.ControllerMatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.json.*;

public class CreateJSONFile {

    private static Logger logAcc = LogManager.getLogger("Account");

    public static void write() {

        JsonArrayBuilder builder = Json.createArrayBuilder();

        List<User> list = User.getUserList();
        for (int i = 0; i < list.size(); i++ ) {

            JsonObjectBuilder buildUser = Json.createObjectBuilder();

            String encodedPassword = Base64.getEncoder().encodeToString(list.get(i).getPassword().getBytes());

            buildUser.add("username", list.get(i).getUsername());
            buildUser.add("password", encodedPassword);
            buildUser.add("points", list.get(i).getPoints());

            JsonObject user = buildUser.build();

            builder.add(user);
        }

        JsonArray userList = builder.build();

        try {
            FileWriter fw = new FileWriter("userList.txt");

            JsonWriter jsonWriter = Json.createWriter(fw);
            jsonWriter.writeArray(userList);
            logAcc.info("File created successfully");

            jsonWriter.close();
            fw.close();

        } catch (IOException e) {
            logAcc.error("Couldn't write file - Information of " + ControllerMatch.getCurrentUser().toString() + " is getting lost");
        }

    }
}



