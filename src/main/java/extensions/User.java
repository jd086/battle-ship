package extensions;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String username;
    private String password;
    private int points;
    private static List<User> userList;

    public User(String username, String password, int points) {
        this.username = username;
        this.password = password;
        this.points = points;
    }

    //Constructor to clone a user object
    public User(User user) {
        username = user.getUsername();
        password = user.getPassword();
        points = user.getPoints();
    }

    @Override
    public String toString() {
        return "Username: " + username + ", Points: " + points;
    }


    public String getUsername() { return username; }

    public String getPassword() { return password; }

    public int getPoints() { return points; }

    public static List<User> getUserList() {
        List<User> temporary = new ArrayList<>();
        for (int i = 0; i < userList.size(); i++) {
            User u = new User(userList.get(i));
            temporary.add(u);
        }
        return temporary;
    }


    public void setPoints(int points) { this.points = points; }

    public static void setUserList(List<User> list) {
        userList = list;
    }


    public boolean verifyPassword(String inputPassword){
        return (inputPassword.equals(password));
    }

}

