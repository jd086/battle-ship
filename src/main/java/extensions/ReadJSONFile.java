package extensions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.json.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class ReadJSONFile {

    private static Logger logAcc = LogManager.getLogger("Account");

    public static void readFile() {

        FileReader fr;
        JsonStructure struct;

        try {
            fr = new FileReader("userList.txt");
            JsonReader reader = Json.createReader(fr);
            struct = reader.read();
            JsonValue value = struct;
            List<User> userList = new ArrayList<>();
            assign(value, userList);

            reader.close();
            fr.close();
        } catch (FileNotFoundException e) {
            logAcc.error("Couldn't find file");
            List<User> list = new ArrayList<>();
            User.setUserList(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void assign(JsonValue value, List<User> userList) {
        JsonObject object;
        if (value.getValueType() == JsonValue.ValueType.OBJECT) {
            object = (JsonObject) value;

            String username = "";
            String password = "";
            int points = 0;
            for (Map.Entry<String, JsonValue> set : object.entrySet()) {
                    if (set.getKey().equals("username")) {
                        username = set.getValue().toString().replace('"', ' ').trim();
                    }
                    if (set.getKey().equals("password")) {
                        byte[] decodedPassword = Base64.getDecoder().decode(set.getValue().toString().replace('"', ' ').trim());
                        password = new String(decodedPassword);

                    }
                    if (set.getKey().equals("points")) {
                        points = Integer.parseInt(set.getValue().toString());
                    }
            }
            User user = new User(username, password, points);
            userList.add(user);

        } else if (value.getValueType() == JsonValue.ValueType.ARRAY) {
            JsonArray array = (JsonArray) value;
            for (JsonValue val : array) {
                assign(val, userList);
            }
            User.setUserList(userList);
            logAcc.info("File successfully read - user list is being set");
        }
    }
}
