package extensions;
import Interfaces.IPlayer;
import core.*;
import exceptions.IllegalFactoryArgument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PlayerFactory {

    private static Logger log = LogManager.getLogger(PlayerFactory.class);

    public static IPlayer createPlayer(IPlayer.PlayerGroups p) throws IllegalFactoryArgument {
        switch(p) {
            case HUMAN:
                return new Human(14);
            case COMPUTER:
                return new Computer();
            default:
                log.error("Wrong player group " + p);
                throw new IllegalFactoryArgument("Wrong player group!");
        }
    }

}

