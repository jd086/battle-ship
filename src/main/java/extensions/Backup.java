package extensions;

import core.FieldType;
import core.GameManager;

public class Backup {

    private FieldType[][] fieldsHuman;
    private FieldType[][] fieldsComputer;
    private GameManager.Score score;
    private int hitsHuman;
    private int hitsComputer;

    public FieldType[][] getFieldsHuman() {
        FieldType[][] temporary = new FieldType[7][7];
        for (int i = 0; i < fieldsHuman.length; i++) {
            temporary[i] = fieldsHuman[i].clone();
        }
        return temporary;
    }

    public FieldType[][] getFieldsComputer() {
        FieldType[][] temporary = new FieldType[7][7];
        for (int i = 0; i < fieldsComputer.length; i++) {
            temporary[i] = fieldsComputer[i].clone();
        }
        return temporary;
    }

    public GameManager.Score getScore() { return score; }

    public int getHitsHuman() { return hitsHuman; }

    public int getHitsComputer() { return hitsComputer; }



    public void setFieldsHuman(FieldType[][] fieldsHuman) { this.fieldsHuman = fieldsHuman; }

    public void setFieldsComputer(FieldType[][] fieldsComputer) { this.fieldsComputer = fieldsComputer; }

    public void setScore(GameManager.Score score) { this.score = score; }

    public void setHitsHuman(int hitsHuman) { this.hitsHuman = hitsHuman; }

    public void setHitsComputer(int hitsComputer) { this.hitsComputer = hitsComputer; }
}
