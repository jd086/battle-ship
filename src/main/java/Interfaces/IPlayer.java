package Interfaces;
import core.FieldType;


public interface IPlayer {

    enum PlayerGroups{

        HUMAN,
        COMPUTER;

        PlayerGroups() { }
    }

    boolean placeShip(int x, int y);
    void removeShip(int x, int y);
    FieldType[][] throwBomb (int x, int y, FieldType[][] fields);
    int getHits();
    FieldType[][] getOwnFields();
    void setOwnFields(FieldType[][] fields);
    int getRoundCounter();
    int getQuantityShips();
}
